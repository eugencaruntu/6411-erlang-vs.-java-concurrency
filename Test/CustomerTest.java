import org.junit.jupiter.api.Test;

class CustomerTest {

    @Test
    void run() {
        Thread thread = new Thread(new Customer("Hello there. I'm running inside a thread", 0));
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }
}