% Area calculation module

-module(test).
-export([area/0, area/1]).

area({square,X}) -> X*X;
area({rectangle,X,Y}) -> X*Y.

area() ->
    receive
        {From, {square,X}} ->
            From ! X*X;
        {From, {rectangle,X,Y}} ->
            From ! X*Y
        end,
        area().