/*
  Author:  eugen caruntu, 29077103
  Title:   A banking environment application with customers borrowing from banks, implemented with concurrent threads
 */

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * @author eugen caruntu, 29077103
 * The customer thread
 */
public class Customer extends Thread {
    private String name;
    private int objective;
    private int balance;
    private Set<String> refusingBanks;

    /**
     * Parameterized constructor
     *
     * @param name      the name of the customer
     * @param objective the initial objective
     */
    Customer(String name, int objective) {
        this.name = name;
        this.balance = objective;
        this.objective = objective;
        this.refusingBanks = new HashSet<>();
    }

    /**
     * The logic for borrowing the money
     */
    private synchronized void borrow() {
        while (balance > 0 && money.banks.size() != refusingBanks.size()) {
            Bank bank = randomBank();
            try {
                sleep(random(10, 100));
                // first prepare an amount considering the balance
                int amount = (balance >= 50) ? random(1, 50) : random(1, balance);

                if (!refusingBanks.contains(bank.name)) {
                    money.responses.put(name + " requests a loan of " + amount + " dollar(s) from " + bank.name);
                    if (bank.lend(name, amount)) {
                        balance -= amount;
                    } else {
                        refusingBanks.add(bank.name);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // send final message
        try {
            if (balance == 0) {
                money.responses.put(name + " has reached the objective of " + objective + " dollar(s). Woo Hoo!");
            } else {
                money.responses.put(name + " was only able to borrow " + (objective - balance) + " dollar(s). Boo Hoo!");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // remove self from customers so that the banks know when no more clients
        money.customers.remove(this);
    }

    /**
     * Produce a random number given an interval
     *
     * @param min the lower-bound
     * @param max the upper-bound
     * @return the random number
     */
    private int random(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }

    /**
     * Obtain a random bank object from the banks collection
     *
     * @return a Bank object
     */
    synchronized private Bank randomBank() {
        Random generator = new Random();
        Object[] allBanks = money.banks.toArray();
        return (Bank) allBanks[generator.nextInt(allBanks.length)];
    }

    /**
     * {@inheritDoc}
     * <p>
     * Keep on borrowing
     */
    @Override
    public void run() {
        synchronized (this) {
            borrow();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @return the name and balance
     */
    @Override
    public String toString() {
        return name + ": " + balance;
    }

}
