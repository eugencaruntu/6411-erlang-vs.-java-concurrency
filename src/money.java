/*
  Author:  eugen caruntu, 29077103
  Title:   A banking environment application with customers borrowing from banks, implemented with concurrent threads
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author eugen caruntu, 29077103
 * The main thread that spins customer and bank threads
 */
public class money extends Thread {
    static Set<Bank> banks = Collections.newSetFromMap(new ConcurrentHashMap<>());
    static Set<Customer> customers = Collections.newSetFromMap(new ConcurrentHashMap<>());
    static BlockingQueue<String> responses = new LinkedBlockingQueue<>();
    static AtomicBoolean running = new AtomicBoolean(true);

    /**
     * Load the input from files.
     *
     * @param fileName the file name
     * @implNote The file names are assumed to be "banks.txt", "customers.txt" and be placed in same folder
     */
    private void readFile(String fileName) {
        File file = new File(fileName);
        try (Scanner sc = new Scanner(file)) {
            while (sc.hasNextLine()) {
                String line = sc.next().replaceAll("[ {}.]", "");
                String[] arr = line.split(",");
                if (fileName.toLowerCase().contains("banks")) {
                    banks.add(new Bank(arr[0], Integer.parseInt(arr[1])));
                } else if (fileName.toLowerCase().contains("customers")) {
                    customers.add(new Customer(arr[0], Integer.parseInt(arr[1])));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Display the set of either banks or customers
     *
     * @param title   the text to be shown first
     * @param objects the set of objects(banks or customers)
     */
    private void display(String title, Set objects) {
        System.out.println(title);
        for (Object o : objects) {
            System.out.println(o.toString());
        }
        System.out.println();
    }

    /**
     * {@inheritDoc}
     * <p>
     * Shows the customers and the banks
     * Then monitor their responses by removing them and printing to console
     * Exits once all bank threads terminate
     */
    @Override
    public void run() {
        synchronized (this) {
            this.display("** Customers and loan objectives **", customers);
            this.display("** Banks and financial resources **", banks);

            while (running.get()) {
                try {
                    System.out.println(responses.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (money.banks.isEmpty() && responses.isEmpty()) {
                    running.set(false);
                }
            }
        }
    }

    /**
     * The main entry point of the application
     *
     * @param args the arguments (expect none)
     */
    public static void main(String[] args) {

        /* Make a money thread and load the input files */
        money m = new money();
        m.readFile("banks.txt");
        m.readFile("customers.txt");

        /* Start the main, banks and customers threads and add them to a collection */
        ArrayList<Thread> threads = new ArrayList<>();
        m.start();
        threads.add(m);
        for (Bank b : banks) {
            b.start();
            threads.add(b);
        }
        for (Customer c : customers) {
            c.start();
            threads.add(c);
        }

        /* Join the threads so the main process knows when they finish in case we need to print something after (not applicable in final solution) */
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
