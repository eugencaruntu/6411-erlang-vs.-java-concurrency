/*
  Author:  eugen caruntu, 29077103
  Title:   A banking environment application with customers borrowing from banks, implemented with concurrent threads
 */

/**
 * @author eugen caruntu, 29077103
 * The bank thread
 */
public class Bank extends Thread {
    String name;
    private int balance;

    /**
     * Parameterized constructor
     *
     * @param name    the name of the bank
     * @param balance the starting balance
     */
    Bank(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    /**
     * The logic of lending the money
     *
     * @param clientName the requesting client
     * @param amount     the requested amount
     * @return true if loan is granted, false otherwise
     */
    boolean lend(String clientName, int amount) {
        try {
            if (amount <= balance) {
                balance -= amount;
                money.responses.put(name + " approves a loan of " + amount + " dollars from " + clientName);
                return true;
            } else {
                money.responses.put(name + " denies a loan of " + amount + " dollars from " + clientName);
                return false;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Spin as long as main process is running and there are still customers threads alive
     * Send a message containing final balance to master thread on exit
     */
    @Override
    public void run() {
        synchronized (this) {
            while (money.running.get()) {
                if (money.customers.isEmpty() && money.banks.contains(this)) {
                    try {
                        money.responses.put(name + " has " + balance + " dollar(s) remaining.");
                        money.banks.remove(this);
                        sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @return the name and balance
     */
    @Override
    public String toString() {
        return name + ": " + balance;
    }

}
