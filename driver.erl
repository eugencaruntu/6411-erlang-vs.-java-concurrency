% Dtiver module
-module(driver).
-export([start/0]).
-import(test, [area/0, area/1]).

start() ->
        Pid = spawn(test, area, []),
    Pid ! {self(), {square, 10}},
        io:fwrite("parent ID: ~w\n\n", [self()]),
        receive
            {Pid, Reply} -> Reply
        end,
    get_feedback().

get_feedback() ->
	receive
		{Msg} -> 
			io:fwrite("Msg in get_feedback ~s \n\n", [Msg])
	end.