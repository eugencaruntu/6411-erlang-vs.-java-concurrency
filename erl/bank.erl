%%%-------------------------------------------------------------------
%%% A banking environment application with customers borrowing from banks
%%% @author eugen caruntu, 29077103
%%% @copyright (C) 2019
%%% Created : 10. Jun 2019 6:19 PM
%%%-------------------------------------------------------------------
-module(bank).
-author("eugen caruntu, 29077103").

%% ====================================================================
%% API functions
%% ====================================================================
-export([account/2]).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% The bank account logic monitoring requests
account(Name, Balance) ->
  receive
    {balance} ->
      main ! {balance, {Name, Balance}}; % exiting process
    {lend, {CustomerPid, CustomerName, Amount}} ->
      if
        Amount =< Balance ->
          main ! {approves, {CustomerName, Name, Amount}},
          CustomerPid ! {ok, Amount},
          account(Name, Balance - Amount);
        true ->
          main ! {denies, {CustomerName, Name, Amount}},
          CustomerPid ! {error, self()},
          account(Name, Balance)
      end
  end.
