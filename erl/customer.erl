%%%-------------------------------------------------------------------
%%% A banking environment application with customers borrowing from banks
%%% @author eugen caruntu, 29077103
%%% @copyright (C) 2019
%%% Created : 10. Jun 2019 6:19 PM
%%%-------------------------------------------------------------------
-module(customer).
-author("eugen caruntu, 29077103").

%% ====================================================================
%% API functions
%% ====================================================================
-export([account/3]).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% The client account instantiation
account(Name, Balance, BankPIDs) ->
  register(Name, self()),
  self() ! {borrow},
  activity(Name, Balance, Balance, BankPIDs, []).

%% Monitor requests and transactions' logic
activity(Name, Desired, Balance, BankPIDs, Rejecting) ->
  receive
    {balance} ->
      if
        Balance == 0 -> main ! {paidoff, {Name, Desired}};
        true -> main ! {remainder, {Name, Desired - Balance}}
      end; % exiting process
    {ok, Amount} ->
      self() ! {borrow},
      activity(Name, Desired, Balance - Amount, BankPIDs, Rejecting);
    {error, BankPid} ->
      self() ! {borrow},
      activity(Name, Desired, Balance, BankPIDs, add_rejecting(BankPid, Rejecting));
    {borrow} ->
      timer:sleep(random_number(10, 100)),
      if
        length(Rejecting) == length(BankPIDs) ->
          self() ! {balance},
          activity(Name, Desired, Balance, BankPIDs, Rejecting);
        Balance =< 0 -> self() ! {balance},
          activity(Name, Desired, Balance, BankPIDs, Rejecting);
        true ->
          % first prepare an amount considering the balance
          if Balance >= 50 -> Max = 50;
            true -> Max = Balance
          end,
          Amount = random_number(0, Max),
          % prepare a list of valid banks
          ValidBankPIDs = lists:subtract([BankPid || {BankPid, _} <- BankPIDs], Rejecting),
          % only consider banks after subtracting rejecting list
          BankPid = lists:nth(random_number(0, length(ValidBankPIDs)), ValidBankPIDs),
          main ! {borrow, {Name, findBankName(BankPid, BankPIDs), Amount}},
          BankPid ! {lend, {self(), Name, Amount}},
          activity(Name, Desired, Balance, BankPIDs, Rejecting)
      end
  end.

%% Produce a random number within an interval
random_number(Min, Max) ->
  rand:uniform(Max - Min) + Min.

%% Find a bank name by its Pid within a list of tuples
findBankName(_, []) -> false;
findBankName(Pid, [{BankPid, Name} | Rest]) ->
  case (Pid == BankPid) of
    true -> Name;
    false -> findBankName(Pid, Rest)
  end.


add_rejecting(BankPid, Rejecting) ->
  case lists:member(BankPid, Rejecting) of
    true -> Rejecting;
    false -> [BankPid | Rejecting]
  end.