%%%-------------------------------------------------------------------
%%% A banking environment application with customers borrowing from banks
%%% @author eugen caruntu, 29077103
%%% @copyright (C) 2019
%%% Created : 10. Jun 2019 6:19 PM
%%%-------------------------------------------------------------------
-module(money).
-author("eugen caruntu, 29077103").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0]).

%% ====================================================================
%% Internal functions
%% ====================================================================

start() ->
  register(main, self()),
  process_flag(trap_exit, true),

  %%  Read the input files and display the content
  {ok, CUSTOMERS} = file:consult("customers.txt"),
  {ok, BANKS} = file:consult("banks.txt"),
  main ! {print_customers, CUSTOMERS},
  main ! {print_banks, BANKS},

  %%  Make accounts for each of the banks and customers
  BankPIDs = [{spawn_link(bank, account, [Name, Balance]), Name} || {Name, Balance} <- BANKS],
  CustomerPIDs = [spawn_link(customer, account, [Name, Balance, BankPIDs]) || {Name, Balance} <- CUSTOMERS],

  echo(CustomerPIDs, BankPIDs).

%% Monitor mesages
echo(CustomerPIDs, BankPIDs) ->
  receive
    {borrow, {CustomerName, BankName, Amount}} ->
      io:format("~p requests a loan of ~p dollar(s) from ~p.~n", [CustomerName, Amount, BankName]);
    {approves, {CustomerName, BankName, Transaction}} ->
      io:format("~p approves a loan of ~p dollars from ~p.~n", [BankName, Transaction, CustomerName]);
    {denies, {CustomerName, BankName, Transaction}} ->
      io:format("~p denies a loan of ~p dollars from ~p.~n", [BankName, Transaction, CustomerName]);
    {paidoff, {Name, Balance}} ->
      io:format("~p has reached the objective of ~p dollar(s). Woo Hoo!.~n", [Name, Balance]);
    {remainder, {Name, Balance}} ->
      io:format("~p was only able to borrow ~p dollar(s). Boo Hoo!.~n", [Name, Balance]);
    {balance, {Name, Balance}} ->
      io:format("~p has ~p dollar(s) remaining.~n", [Name, Balance]);
    {print_customers, List} ->
      io:format("** Customers and loan objectives **~n"),
      display(List);
    {print_banks, List} ->
      io:format("** Banks and financial resources **~n"),
      display(List);
    {'EXIT', FromPid, _} ->
%%      io:format("Process ~p is exiting.~n", [FromPid]),
      case lists:member(FromPid, CustomerPIDs) of

        % if exiting process is a customer, delete it from list
        true ->
          RemainingCustomers = lists:delete(FromPid, CustomerPIDs),
          case RemainingCustomers of
            [] -> close_banks(BankPIDs),            % if no more customers, close the banks
              echo(RemainingCustomers, BankPIDs);
            _ -> echo(RemainingCustomers, BankPIDs) % still have customers, we continue
          end;

        % if exiting process is not a customer
        false ->
          echo(CustomerPIDs, BankPIDs)

      end;
  % any lost message to take care of
    Msg -> io:format("~p.~n", [Msg])
  after 100 -> init:stop() % stop main process when no activity
  end,
  echo(CustomerPIDs, BankPIDs).

close_banks([]) -> {};
close_banks(BankPIDs) ->
  [{BankPid, _} | Rest] = BankPIDs,
  BankPid ! {balance},
  close_banks(Rest).

display([]) ->
  io:format("~n");
display([{Name, Value} | Rest]) ->
  io:format("~p: ~p~n", [Name, Value]),
  display(Rest).
